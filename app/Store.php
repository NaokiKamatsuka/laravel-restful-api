<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	public function items() {
		return $this->hasMany('App\Item');
	}
}
