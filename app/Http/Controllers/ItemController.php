<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Validator;

class ItemController extends Controller
{
	//商品データを全て返す
    public function index($id)
    {
		$items = Item::where('store_id', $id)->get();
		return response()->json($items);
    }

	//商品詳細データを返す
    public function detail($id)
    {
		$item = Item::where('id', $id)->first();
		return response()->json($item);
    }

	//検索
    public function search(Request $request)
    {
		//検索条件から取得するデータの絞り込み
		//何も入力されなかった場合全件返す
		$items_query = Item::query();

		//ポストされたデータが存在する場合に、whereの条件を追加する
		if ($request->title) {
			    $items_query = $items_query->where('title', 'like', '%' . $request->title . '%');
		}

		if ($request->detail) {
			    $items_query = $items_query->where('detail', 'like', '%' . $request->detail . '%');
		}

		if ($request->min_price) {
			    $items_query = $items_query->where('price', '>=' , $request->min_price);
		}

		if ($request->max_price) {
			    $items_query = $items_query->where('price', '<=' , $request->max_price);
		}

		$items = $items_query->get();
		return response()->json($items);
    }

	//登録
    public function register(Request $request)
    {
		//バリデーション
		$validated_data = $request->validate([
			'title' => 'required|max:100',
			'detail' => 'required|max:500',
			'price' => 'required',
			'img' => 'required|:jpeg,png,jpg,gif|max:2048',
			'store' => 'required',
		]);

		//挿入
		Item::insert([
			'img' => $request->img,
			'title' => $request->title,
			'detail' => $request->detail,
			'price' => $request->price,
			'store_id' => (int)$request->store,
		]);

		$items = Item::all();
		return response()->json($items);
    }

	//変更
    public function update(Request $request, $id)
    {
		//バリデーション
		$validated_data = $request->validate([
			'title' => 'required|max:100',
			'detail' => 'required|max:500',
			'price' => 'required',
			'img' => 'required|:jpeg,png,jpg,gif|max:2048',
		]);

		//変更実行
		Item::where('id', $id)
			->update([
				'title' => $request->title,
				'img' => $request->img,
				'detail' => $request->detail,
				'price' => $request->price,
			]);

		$items = Item::all();
		return response()->json($items);
    }

	//削除(論理削除)
    public function delete($id)
    {
		Item::where('id', $id)->delete();

		$items = Item::all();
		return response()->json($items);
    }
}
