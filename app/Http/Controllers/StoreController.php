<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use Validator;

class StoreController extends Controller
{
	//店舗データを全て返す
    public function index()
    {
		$stores = Store::all();
		return response()->json($stores);
    }
}
