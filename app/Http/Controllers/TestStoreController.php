<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestStoreController extends Controller
{
	//店舗一覧表示
	public function index() {

		$url = route('stores.index');

		//curlを初期化
		$ch = curl_init();

		//設定
		curl_setopt($ch, CURLOPT_URL, $url); //送り主
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //実行結果の取得

		//実行
		$res =  curl_exec($ch);
		//json型のデータをオブジェクト型にデコード
		$stores = json_decode($res);

		//リソースを閉じる
		curl_close($ch);
		return view('test_store_index', compact('stores'));
	}
}
