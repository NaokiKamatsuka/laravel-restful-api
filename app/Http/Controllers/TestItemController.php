<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class TestItemController extends Controller
{
	//商品一覧表示
	public function index($id) {
		$url = route('items.index', ['id' => $id]);

		// curlを初期化
		$ch = curl_init();

		//設定
		curl_setopt($ch, CURLOPT_URL, $url); //送り主
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //実行結果の取得

		//実行
		$res =  curl_exec($ch);
		///json型のデータをオブジェクト型にデコー
		$items = json_decode($res);

		//リソースを閉じる
		curl_close($ch);
		return view('test_item_index', compact('items'));
	}

	//商品詳細表示
	public function detail($id) {
		$url = route('items.detail', ['id' => $id]);

		// curlを初期化
		$ch = curl_init();

		//設定
		curl_setopt($ch, CURLOPT_URL, $url); //送り先
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //実行結果取得の設定

		//実行
		$res =  curl_exec($ch);
		//json型のデータをオブジェクト型にデコード
		$item = json_decode($res);

		//リソースを閉じる
		curl_close($ch);
		return view('test_item_detail', compact('item'));
	}

	//検索フォーム
	public function searchForm() {
		return view('test_search');
	}

	//検索結果表示
	public function search() {
		$url = route('items.search');
		$data = [];

		// $dataに送るデータを詰め
		$data['title'] = $_POST['title'];
		$data['detail'] = $_POST['detail'];
		$data['min_price'] = $_POST['min_price'];
		$data['max_price'] = $_POST['max_price'];

		// 送信データをURLエンコード
		$data = http_build_query($data, "", "&");

		// curlを初期化
		$ch = curl_init();

		// 設定
		curl_setopt($ch, CURLOPT_URL, $url); // 送り先
		curl_setopt($ch, CURLOPT_POST, true); // POST
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // 送信データ
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 実行結果取得の設定

		// 実行
		$output = curl_exec($ch);
		$items = json_decode($output);

		// リソースを閉じる
		curl_close($ch);
		return view('test_search_result', compact('items'));
	}

	//商品登録
	public function register(Request $request) {
		$url = route('items.register');
		$data = [];

		// $dataに送るデータを詰め
		$data['img'] = $request->img;
		$data['title'] = $request->title;
		$data['detail'] = $request->detail;
		$data['price'] = $request->price;
		$data['store'] = $request->store;

		// 送信データをURLエンコード
		$data = http_build_query($data, "", "&");

		// curlを初期化
		$ch = curl_init();

		// 設定
		curl_setopt($ch, CURLOPT_URL, $url); // 送り先
		curl_setopt($ch, CURLOPT_POST, true); // POST
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // 送信データ
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 実行結果取得の設定

		// 実行
		$output = curl_exec($ch);

		// リソースを閉じる
		curl_close($ch);
		//商品一覧ページにリダイレクト
		return redirect()->route('test_stores.index');
	}

	//登録フォーム
	public function registerForm() {
		return view('test_register');
	}

	//更新（未完成)
	public function update() {
		return view('test_update');
	}

	//削除(論理削除)
	public function delete() {
		return view('test_delete');
	}
}
