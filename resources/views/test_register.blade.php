<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品登録</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<script>
$( function() {
    $('#ajax-button').click(
    function() {
        var hostUrl= @json(route('test_register'));
		var img = $_POST['img'];
		var title = $_POST['title'];
		var detail = $_POST['detail'];
		var price = $_POST['price'];
        $.ajax({
            url: hostUrl,
            type:'POST',
            dataType: 'json',
            data : {img : img, title : title, detail : detail, price : price },
        }).done(function(data) {
                          alert("ok");
        }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
                         alert("error");
        })
    });
} );
</script>
</head>
<body>
<h3 id="title">商品登録</h3>
<div class="form">
<form action="{{ route('test_register') }}" method="post">
{{ csrf_field() }}

<p>商品画像</p>
<input type="file" name="img" required>
<br />
<p>商品名</p>
<input type="text" name="title" required>
<br />
<p>説明文</p>
<input type="text" name="detail" required>
<br />
<p>価格</p>
<input type="number" name="price" required>
<br />
<p>販売店舗</p>
<select name="store">
<option value="1">エイトイレブン</option>
<option value="2">フレンドマート</option>
<option value="3">ローソソ</option>
</select>
</br>
<input type="submit" value="登録">
</form>
</div>

</body>
</html>
