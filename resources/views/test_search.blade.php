<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品検索</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<h3 id="title">商品検索</h3>
<form class="form" action="{{ route('test_search') }}" method="post">
{{ csrf_field() }}

<p>商品名</p>
<input type="text" name="title">
<br />
<p>詳細</p>
<input type="text" name="detail">
<br />
<p>価格限度(最小)</p>
<input type="number" name="min_price">
<br />
<p>価格限度(最大)</p>
<input type="number" name="max_price">
<br />
<input type="submit" value="検索">
</form>
</body>
</html>

