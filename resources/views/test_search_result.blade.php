<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>検索結果</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<h3 id="title">商品一覧</h3>
@if (empty($items))
<div class="search_result">
一致する商品は見つかりませんでした。
</div>
@else
<div class="search_result">
@foreach ($items as $item)
<a href="{{ route('test_detail', ['id' => $item->id]) }}">{{ $item->title }}</a>
</br>
@endforeach
</div>
@endif
</body>
</html>
