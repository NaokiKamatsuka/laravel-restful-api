<h3>変更</h3>
<form action="{{ route('items.update', ['id' => 1]) }}" method="get">
{{ csrf_field() }}

<p>商品画像</p>
<input type="file" name="img">
<br />
<p>商品名</p>
<input type="text" name="title">
<br />
<p>説明文</p>
<input type="text" name="detail">
<br />
<p>価格</p>
<input type="number" name="price">
<br />
<input type="submit" value="登録">
</form>

