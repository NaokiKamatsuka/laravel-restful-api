<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>店舗一覧</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div class="stores">
<h3 id="title">店舗一覧から商品を探す</h3>
@foreach ($stores as $store)
<a href="{{ route('test_items.index', ['id' => $store->id]) }}">{{ $store->name }}</a>
</br>
@endforeach
<h3 id="title">新たに商品を登録・商品を検索</h3>
<a href="{{ route('test_register') }}">商品登録</a>
<a href="{{ route('test_search') }}">商品検索</a>
</div>
</body>
</html>
