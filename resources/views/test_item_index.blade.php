<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品一覧</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<div class="stores">
<h3 id="title">商品一覧</h3>
@foreach ($items as $item)
<a href="{{ route('test_detail', ['id' => $item->id]) }}">{{ $item->title }}</a>
</br>
@endforeach
</div>
</body>
</html>
