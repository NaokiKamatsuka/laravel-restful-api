<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>商品詳細</title>
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
<h3 id="title">商品詳細</h3>
<div class="item_detail">
<p>{{ $item->img }}</p>
<p>商品名 : {{ $item->title }}<p>
<p>詳細 : {{ $item->detail }}</p>
<p>値段 : {{ $item->price }}円</p>
</br>
</div>
</body>
</html>
