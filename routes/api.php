<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//商品データを全て返す
Route::get('items/{id}', 'ItemController@index')->name('items.index');
//検索
Route::post('search', 'ItemController@search')->name('items.search');
//登録
Route::post('register', 'ItemController@register')->name('items.register');
//変更
Route::get('update/{id}', 'ItemController@update')->name('items.update');
//削除
Route::get('delete/{id}', 'ItemController@delete')->name('items.delete');

//店舗データを返す
Route::get('stores', 'StoreController@index')->name('stores.index');
//商品詳細データを返す
Route::get('items/detail/{id}', 'ItemController@detail')->name('items.detail');
