<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//商品検索フォーム
Route::get('/test/search', 'TestItemController@searchForm')->name('test_search_form');
//商品検索結果
Route::post('/test/search', 'TestItemController@search')->name('test_search');
//商品登録フォーム
Route::get('/test/register', 'TestItemController@registerForm')->name('test_register_form');
//商品登録
Route::post('/test/register', 'TestItemController@register')->name('test_register');
//商品情報変更フォーム
Route::get('/test/update', 'TestItemController@update')->name('test_update');
//商品削除フォーム
Route::get('/test/delete', 'TestItemController@delete')->name('test_delete');

//店舗一覧表示画面
Route::get('/test/stores', 'TestStoreController@index')->name('test_stores.index');
//店舗ごと商品一覧表示画面
Route::get('/test/items/{id}', 'TestItemController@index')->name('test_items.index');
//商品詳細表示画面
Route::get('/test/items/detail/{id}', 'TestItemController@detail')->name('test_detail');
